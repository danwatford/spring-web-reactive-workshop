export class Mememon {

  private mememonImagePath: String;

  static fromJS(data: any): Mememon {
    return new Mememon(data.id, data.name, data.imageId, data.strength);
  }

  constructor( private id: number, private name: string, private  imageId: string, private strength: number) {
    this.mememonImagePath = '../../assets/images/mememons/' + imageId + '.jpg';
  }
}
