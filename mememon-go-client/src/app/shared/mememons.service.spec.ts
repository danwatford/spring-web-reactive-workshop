/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MememonsService } from './mememons.service';

describe('Service: Mememons', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MememonsService]
    });
  });

  it('should ...', inject([MememonsService], (service: MememonsService) => {
    expect(service).toBeTruthy();
  }));
});
