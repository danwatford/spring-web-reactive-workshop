import { Injectable } from '@angular/core';
import {Mememon} from "./mememon";
import {List} from "immutable";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable()
export class MememonsService {

  private mememonsList: List<Mememon> = List<Mememon>();
  private _mememons: BehaviorSubject<List<Mememon>> = new BehaviorSubject<List<Mememon>>(List<Mememon>());

  constructor() { }

  get mememonsObservable(): Observable<List<Mememon>> {
    return this._mememons.asObservable();
  }

  get mememons(): List<Mememon> {
    return this._mememons.getValue();
  }

  caughtMememon(mememon: Mememon) {
    this.mememonsList = this.mememonsList.push(mememon);
    this._mememons.next(this.mememonsList);
  }

  getMememonsCaught(): List<Mememon> {
    return this.mememonsList;
  }

}
