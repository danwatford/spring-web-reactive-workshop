import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MatButtonModule} from "@angular/material";
import {MatCardModule} from "@angular/material";
import {MatToolbarModule} from '@angular/material';

import {AppComponent} from './app.component';
import {MememonFieldComponent} from './mememon-field/mememon-field.component';
import {MemedexComponent} from './memedex/memedex.component';
import {MememonsService} from "./shared/mememons.service";


@NgModule({
  declarations: [
    AppComponent,
    MememonFieldComponent,
    MemedexComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule
  ],
  providers: [MememonsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
