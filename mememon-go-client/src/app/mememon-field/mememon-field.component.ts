import {Component, OnInit, SkipSelf} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {MememonsService} from "../shared/mememons.service";
import {Mememon} from "../shared/mememon";

@Component({
  selector: 'app-mememon-field',
  templateUrl: './mememon-field.component.html',
  styleUrls: ['./mememon-field.component.css'],
  providers: [MememonsService]
})
export class MememonFieldComponent implements OnInit {

  mememonAppeared: boolean;
  catchFailed: boolean;
  mememon: any;
  dead: boolean;

  constructor(private http: HttpClient, @SkipSelf() private mememonsService: MememonsService) { }

  ngOnInit() {
    this.mememonAppeared = false;
    this.dead = false;

    const mememons = Observable.create(observer => {
      const eventSource = new EventSource('http://localhost:8080/api/encounters');
      eventSource.onmessage = x => observer.next(x.data);
      eventSource.onerror = () => {
        // eventSource was probably closed
        eventSource.close();
      };

      return () => {
        eventSource.close();
      };
    });

    const mememonfield = document.getElementById('mememon-field');
    Observable.fromEvent(mememonfield, 'mousemove')
      .filter(() => !this.mememonAppeared)
      .subscribe((e: any) => {
        const headers = new HttpHeaders({'Content-Type': 'application/json'});

        const data = JSON.stringify({
          x: e.clientX,
          y: e.clientY
        });

        this.http.post('http://localhost:8080/api/location', data, {headers: headers}).subscribe();
    });

    mememons.flatMap(mememon => this.http.get('http://localhost:8080/api/mememon/' + mememon))
      .subscribe(mememon => {
        this.mememonAppeared = true;
        this.mememon = Mememon.fromJS(mememon);
    });
  }

  run() {
    if (this.mememon.strength < 100) {
      this.mememonAppeared = false;
      this.catchFailed = false;
    } else {
      this.dead = true;
    }
  }

  catchMememon() {
    this.catchFailed = false;
    this.http.post('http://localhost:8080/api/mememon/catch/' + this.mememon.id, {})
      .subscribe(caught => {
        if(caught) {
          this.mememonAppeared = false;
          this.mememonsService.caughtMememon(Mememon.fromJS(this.mememon));
        } else {
          this.catchFailed = true;
        }
    })
  }
}
