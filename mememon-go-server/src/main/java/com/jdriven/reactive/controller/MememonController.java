package com.jdriven.reactive.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import com.jdriven.reactive.domain.Mememon;
import com.jdriven.reactive.service.MememonService;
import reactor.core.publisher.Mono;

/**
 * Link these REST endpoints to methods in the {@link MememonService}.
 */
@RestController
@RequestMapping(value = "api")
@CrossOrigin(origins = "http://localhost:4200")
public class MememonController {

    private MememonService mememonService;

    @Autowired
    public MememonController(MememonService mememonService) {
        this.mememonService = mememonService;
    }

    /**
     * Get a Mememon by ID.
     *
     * @param id the ID of the Mememon
     * @return A Mono of a Mememon, or an empty Mono if not found
     */
    @GetMapping(value = "mememon/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Mememon> getMememon(@PathVariable("id") long id) {
        return Mono.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * Try to catch a Mememon. Use the appropriate method in {@link MememonService}
     * to determine if the catch was successful.
     *
     * @param id the ID of the Mememon to catch
     * @return A Mono of a Boolean, indicating if the catch was successful
     */
    @PostMapping(value = "mememon/catch/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Boolean> catchMememon(@PathVariable("id") long id) {
        return Mono.error(new RuntimeException("TODO: implement me!"));
    }
}
