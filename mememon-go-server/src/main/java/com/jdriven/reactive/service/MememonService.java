package com.jdriven.reactive.service;

import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jdriven.reactive.dao.MememonDao;
import com.jdriven.reactive.domain.Mememon;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Our Mememon service. This links our data layer ({@link MememonDao}) to our REST controllers.
 */
@Service
public class MememonService {

    private static final Random RANDOM = new Random();

    private MememonDao mememonDao;

    @Autowired
    public MememonService(MememonDao mememonDao) {
        this.mememonDao = mememonDao;
    }

    public Flux<Mememon> getAllMememons() {
        return mememonDao.getAll();
    }

    public Mono<Mememon> getById(Mono<Long> id) {
        return mememonDao.getById(id);
    }

    /**
     * For a given Mememon Mono, returns a Boolean Mono indicating if the catch is successful.
     * To determine if we can successfully catch a Mememon, use the below {@link #canCatch(Mememon)} method.
     *
     * @param mememon Mono of the Mememon to catch
     * @return Mono of a Boolean indicating if the catch was successful
     */
    public Mono<Boolean> catchMememon(Mono<Mememon> mememon) {
        return Mono.error(new RuntimeException("TODO: implement me!"));
    }

    private boolean canCatch(Mememon mememon) {
        return (RANDOM.nextInt(100) + 1) > mememon.getStrength();
    }

}
