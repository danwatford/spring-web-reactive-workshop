package com.jdriven.reactive.dao;

import org.springframework.stereotype.Repository;
import com.jdriven.reactive.domain.Mememon;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Our 'reactive' database layer. To implement the below methods,
 * use the {@link MememonDatabase} as a data source.
 */
@Repository
public class MememonDao {

    /**
     * Returns all the Mememon from the {@link MememonDatabase} as a Flux.
     * @return Flux of Mememons
     */
    public Flux<Mememon> getAll() {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * Return the Mememon with the given ID as a Mono.
     * Should return an empty Mono if no result is found.
     *
     * @param idMono Mono of the ID to search for
     * @return Mono of a Mememon
     */
    public Mono<Mememon> getById(Mono<Long> idMono) {
        return Mono.error(new RuntimeException("TODO: implement me!"));
    }
}
