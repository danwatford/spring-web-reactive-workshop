package com.jdriven.reactive.domain;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

public class Mememon implements Serializable, Comparable<Mememon> {

    private long id;
    private String name;
    private String imageId;
    private int strength;

    public Mememon(long id, String name, String imageId, int strength) {
        this.id = id;
        this.name = name;
        this.imageId = imageId;
        this.strength = strength;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImageId() {
        return imageId;
    }

    public int getStrength() {
        return strength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Mememon mememon = (Mememon) o;
        return this.id == mememon.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public int compareTo(Mememon o) {
        return Objects.compare(this.id, o.id, Comparator.naturalOrder());
    }
}
