package com.jdriven.reactive.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class MememonDaoTest {

    private MememonDao mememonDao;

    @Before
    public void setUp() {
        this.mememonDao = new MememonDao();
    }

    @Test
    public void shouldGetAll() {
        StepVerifier.create(mememonDao.getAll())
                .expectNextCount(11)
                .verifyComplete();
    }

    @Test
    public void shouldGetExistingById() {
        StepVerifier.create(mememonDao.getById(Mono.just(5L)))
                .assertNext(m -> assertEquals(5L, m.getId()))
                .verifyComplete();
    }

    @Test
    public void shouldGetEmptyMonoForNonExistingId() {
        StepVerifier.create(mememonDao.getById(Mono.just(-1L)))
                .verifyComplete();
    }
}
