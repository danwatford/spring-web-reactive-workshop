package com.jdriven.reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveSpringIntroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveSpringIntroApplication.class, args);
	}
}
