package com.jdriven.reactive.exercises;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Test;


import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

/**
 * Filter the stream of items emitted by Flux
 */
public class E2_FilterTest {

    /**
     * From the List of Integers filter keep the uneven numbers only
     *
     * @param list List of Integer
     * @return Flux of Integer
     */
    public Flux<Integer> filter(final List<Integer> list) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * From the List of Strings take the first two
     *
     * @param list List of String
     * @return Flux of String
     */
    public Flux<String> take(final List<String> list) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * From the List of Integers return that list without the first [count] elements.
     *
     * @param list List of Integer
     * @return Flux of Integer
     */
    public Flux<Integer> skip(final List<Integer> list, final Integer count) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * From the List of Integers list remove all duplicates.
     *
     * @param list List of Integer
     * @return Flux of Integer
     */
    public Flux<Integer> distinct(final List<Integer> list) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }


    //
    // -------------------------------------------------------------------
    //


    @Test
    public void filterTest() {
        StepVerifier.create(filter(range(1, 10)))
                .expectNext(1, 3, 5, 7, 9)
                .verifyComplete();
    }

    @Test
    public void takeTest() {
        StepVerifier.create(take(Arrays.asList("One", "Two", "Three", "Four")))
                .expectNext("One", "Two")
                .verifyComplete();
    }

    @Test
    public void skipTest() {
        StepVerifier.create(skip(range(1, 10), 5))
                .expectNext(6, 7, 8, 9)
                .verifyComplete();
    }

    @Test
    public void distinctTest() {
        final List<Integer> check = IntStream
                .concat(IntStream.range(1, 10), IntStream.range(1, 10))
                .boxed()
                .collect(Collectors.toList());
        StepVerifier.create(distinct(check))
                .expectNext(1, 2, 3, 4, 5, 6, 7, 8, 9)
                .verifyComplete();
    }



    private static List<Integer> range(final Integer lo, final Integer hi) {
        return IntStream.range(lo, hi).boxed().collect(Collectors.toList());
    }
}
