package com.jdriven.reactive.exercises;

import static reactor.core.publisher.Flux.error;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;


import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

/**
 * Just a brief first sniff of the resilience facilities.
 */
public class E4_ResilienceTest {

    /**
     * Leave the incoming data as is, but handle errors by returning a 'default-value'.
     *
     * @param data Flux of String
     * @return Flux of String
     */
    public Flux<String> handleError(Flux<String> data) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * The incoming data stream can have failures, so return that stream with added retry handling.
     *
     * @param data Flux of String
     * @return Flux of String
     */
    public Flux<String> retry(Flux<String> data) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }


    //
    // -------------------------------------------------------------------
    //


    @Test
    public void handleErrorTest() {
        StepVerifier.create(handleError(error(new RuntimeException("failure"))))
                .expectNext("default-value")
                .verifyComplete();
    }

    @Test
    public void retryTest() {
        AtomicInteger c = new AtomicInteger();
        Flux<String> o = Flux.create(s -> {
            if (c.incrementAndGet() <= 1) {
                s.error(new RuntimeException("fail"));
            } else {
                s.next("success!");
                s.complete();
            }
        });
        StepVerifier.create(retry(o))
                .expectNext("success!")
                .verifyComplete();
    }
}
