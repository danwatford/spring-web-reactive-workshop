package com.jdriven.reactive.exercises;

import static org.junit.Assert.assertTrue;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;

import org.junit.Test;


import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

/**
 * Special (optional) assignments, bit more difficult than the previous exercises...
 */
public class E5_SpecialsTest {

    /**
     * The Project Euler #1: return the sum of the list of integers,
     * starting from 1 until [count] (inclusive), excluding the integers that are not dividable by 3 or 5.
     * See: https://projecteuler.net/problem=1
     *
     * @param count Integer where to stop...
     * @return Mono of Integer
     */
    public Mono<Integer> eulerOne(final Integer count) {
        return Mono.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * Replace each number divisable by three with 'fizz' and each number divisable by five with 'buzz'.
     * See: http://en.wikipedia.org/wiki/Fizz_buzz for more information.
     *
     * @param count Integer the number to count to
     * @return Flux of String
     */
    public Flux<String> fizzBuzz(final Integer count) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * Create a flux that emits "tick!" every second (= 1000 millisecond events)
     *
     * hint: start with <code>time().</code>
     *
     * @return Flux of String
     */
    public Flux<String> tick() {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }


    //
    // -------------------------------------------------------------------
    //

    /**
     * Returns a Flux that emits the current time every millisecond.
     */
    private static Flux<LocalTime> time() {
        Clock clock = Clock.fixed(Instant.parse("2000-01-01T12:00:00.00Z"), ZoneId.systemDefault());
        return Flux.interval(Duration.ofMillis(1)).map(i -> LocalTime.now(Clock.offset(clock, Duration.ofMillis(i))));
    }


    @Test
    public void eulerOneTest() {
        StepVerifier.create(eulerOne(100))
                .expectNext(2418)
                .verifyComplete();
    }

    @Test
    public void fizzBuzzTest() {
        StepVerifier.create(fizzBuzz(21))
                .expectNext("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11",
                        "Fizz", "13", "14", "FizzBuzz", "16", "17", "Fizz", "19", "Buzz", "Fizz")
                .verifyComplete();
    }

    @Test
    public void tickTest() {
        LocalTime start = LocalTime.now();
        StepVerifier.create(tick().take(3))
                .expectNext("tick!", "tick!", "tick!")
                .verifyComplete();
        LocalTime end = LocalTime.now();
        Duration elapsed = Duration.between(start, end);
        assertTrue("Collecting the ticks took longer than expected!", elapsed.getSeconds() < 4);
        assertTrue("Collecting ticks was faster than expected", elapsed.getSeconds() > 1);

    }
}
